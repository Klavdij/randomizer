package rand;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;


public class Podatki {
	
	private GregorianCalendar datum;
	private double lokacija_x;
	private double lokacija_y;
	
	// Tipi (s)ms, (m)ms, (k)lic, (n)et
	private char tip;
	private int odhDoh;
	private int stevilka;
	
	// V sekundah
	private int trajanje;
	
	// V kB
	private int kolicina;

	/**
	 * Konstruktor za SMS
	 * @param datum
	 * @param lokacija_x
	 * @param lokacija_y
	 * @param tip
	 * @param odhDoh
	 * @param stevilka
	 */
	public Podatki(GregorianCalendar datum, double lokacija_x,
			double lokacija_y, char tip, int odhDoh, int stevilka) {
		super();
		this.datum = datum;
		this.lokacija_x = lokacija_x;
		this.lokacija_y = lokacija_y;
		this.tip = tip;
		this.odhDoh = odhDoh;
		this.stevilka = stevilka;
	}
	
	/**
	 * Konstruktor za NET
	 * @param datum
	 * @param lokacija_x
	 * @param lokacija_y
	 * @param tip
	 * @param kolicina
	 */
	public Podatki(GregorianCalendar datum, double lokacija_x,
			double lokacija_y, char tip, int kolicina) {
		super();
		this.datum = datum;
		this.lokacija_x = lokacija_x;
		this.lokacija_y = lokacija_y;
		this.tip = tip;
		this.kolicina = kolicina;
	}

	/**
	 * Konstruktor za KLICE
	 * @param datum
	 * @param lokacija_x
	 * @param lokacija_y
	 * @param tip
	 * @param odhDoh
	 * @param stevilka
	 * @param trajanje
	 */
	public Podatki(GregorianCalendar datum, double lokacija_x,
			double lokacija_y, char tip, int odhDoh, int stevilka, int trajanje) {
		super();
		this.datum = datum;
		this.lokacija_x = lokacija_x;
		this.lokacija_y = lokacija_y;
		this.tip = tip;
		this.odhDoh = odhDoh;
		this.stevilka = stevilka;
		this.trajanje = trajanje;
	}


	public Date getDatum() {
		return datum.getTime();
	}


	public double getLokacija_x() {
		return lokacija_x;
	}


	public double getLokacija_y() {
		return lokacija_y;
	}


	public char getTip() {
		return tip;
	}


	public int getOdhDoh() {
		return odhDoh;
	}


	public int getStevilka() {
		return stevilka;
	}


	public int getTrajanje() {
		return trajanje;
	}


	public int getKolicina() {
		return kolicina;
	}


	@Override
	public String toString() {
		return nizDatum() + ", lokacija_x=" + lokacija_x
				+ ", lokacija_y=" + lokacija_y + ", tip=" + tip + ", odhDoh="
				+ odhDoh + ", stevilka=" + stevilka + ", trajanje=" + trajanje
				+ ", kolicina=" + kolicina;
	}
	
	private String nizDatum(){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		
		return df.format(datum.getTime());
	}
	
}
