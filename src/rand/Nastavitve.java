package rand;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;


// Nastavitve nakljucnega generiranja
public class Nastavitve {
	
	private Properties prop;
	
	public Nastavitve() {
		prop=new Properties();
		try {
			// Nalozi datoteko
			prop.load(new FileInputStream("config.properties"));
			
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public Nastavitve(String nastNiz){
		
		prop=new Properties();
		
		try {
			prop.load(new StringReader(nastNiz));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}
	
	public String getProp(String s){
		return prop.getProperty(s);
	}
	
	public int getPropInt(String s){
		return Integer.parseInt(prop.getProperty(s));
	}
	
	public double getPropDouble(String s){
		return Double.parseDouble(prop.getProperty(s));
	}
	
	public double[] getPropDoubleArr(String s){
		String [] sa=prop.getProperty(s).split(",");
		double [] arr=new double [sa.length];
		
		for (int i = 0; i < arr.length; i++) {
			arr[i]=Double.parseDouble(sa[i]);
		}
		
		return arr;
	}
	
}
