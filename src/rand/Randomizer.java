package rand;

import java.util.Arrays;
import java.util.GregorianCalendar;


public class Randomizer {
	
	private static Nastavitve nast;
	private static int [] omrezjeUpo;
	
	public static void main(String[] args) {
		nast=new Nastavitve();
		start();
	}
	
	public static void generiraj(String n){
		nast=new Nastavitve(n);
		start();
	}

	private static void start() {
				
		Baza baza=new Baza(nast.getProp("EMSO"));
		
		omrezjeUpo=baza.omrezjeUporabnika();
		
		Podatki nakljucniSMS []=new Podatki[nast.getPropInt("SMS_ST")];
		Podatki nakljucniMMS []=new Podatki[nast.getPropInt("MMS_ST")];
		Podatki nakljucniKlici []=new Podatki[nast.getPropInt("KLICI_ST")];
		Podatki nakljucniNET []=new Podatki[nast.getPropInt("NET_ST")];
		
		nakljucnoGenerirajSMS(nakljucniSMS);
		nakljucnoGenerirajMMS(nakljucniMMS);
		nakljucnoGenerirajKlice(nakljucniKlici);
		nakljucnoGenerirajNET(nakljucniNET);
		
		baza.vnesiPodatke(nakljucniSMS);
		baza.vnesiPodatke(nakljucniMMS);
		baza.vnesiPodatke(nakljucniKlici);
		baza.vnesiPodatke(nakljucniNET);
		
		baza.disconnect();
		
	}
	
	private static void nakljucnoGenerirajSMS(Podatki [] p){
		GregorianCalendar datum;
		int stevilka;
		int odhDoh;
		
		for (int i = 0; i<p.length; i++) {
			datum=nakljucniDatum();
			stevilka=nakljucnaStevilka('s');
			odhDoh=randomRange(0, 2);
			
			p[i]=new Podatki(datum, nast.getPropDouble("LOKACIJA_X"), nast.getPropDouble("LOKACIJA_Y"), 's', odhDoh, stevilka);
			
		}
	}
	
	private static void nakljucnoGenerirajMMS(Podatki [] p){
		GregorianCalendar datum;
		int stevilka;
		int odhDoh;
		
		for (int i = 0; i<p.length; i++) {
			datum=nakljucniDatum();
			stevilka=nakljucnaStevilka('s');
			odhDoh=randomRange(0, 2);			
			
			p[i]=new Podatki(datum, nast.getPropDouble("LOKACIJA_X"), nast.getPropDouble("LOKACIJA_Y"), 'm', odhDoh, stevilka);
			
		}
	}
	
	private static void nakljucnoGenerirajNET(Podatki [] p){
		GregorianCalendar datum;
		int kolicina;
		
		for (int i = 0; i<p.length; i++) {
			datum=nakljucniDatum();
			kolicina=nakljucnaKolicina();		
			
			p[i]=new Podatki(datum, nast.getPropDouble("LOKACIJA_X"), nast.getPropDouble("LOKACIJA_Y"), 'n', kolicina);
			
		}
	}
	
	private static void nakljucnoGenerirajKlice(Podatki [] p){
		GregorianCalendar datum;
		int stevilka;
		int odhDoh;
		int trajanje;
		
		for (int i = 0; i<p.length; i++) {
			datum=nakljucniDatum();
			stevilka=nakljucnaStevilka('k');
			odhDoh=randomRange(0, 2);
			trajanje=nakljucnoTrajanje();			
			
			p[i]=new Podatki(datum, nast.getPropDouble("LOKACIJA_X"), nast.getPropDouble("LOKACIJA_Y"), 'k', odhDoh, stevilka, trajanje);
			
		}
	}

	private static GregorianCalendar nakljucniDatum(){
		GregorianCalendar d;
		
		int leto=nast.getPropInt("LETO");
		
		//Dobimo mesec
		int mesec=0;
		double r=Math.random();
		
		double [] uteziMeseci=nast.getPropDoubleArr("UTEZI_MESECI");
		double utez=0.0;
		
		for (int i = 0; i < uteziMeseci.length; i++) {
			
			if(r>=utez && r<utez+uteziMeseci[i]){
				mesec=i;
				break;
			}
			
			utez+=uteziMeseci[i];
		}
		
		//Dobimo dan
		int dan=1;
		r=Math.random();
		double uteziMesec=nast.getPropDouble("UTEZI_MESEC");

		GregorianCalendar cal=new GregorianCalendar(leto, mesec, 1);
		int days=cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		
		if(r<uteziMesec){
			dan=randomRange(1, 16);
		}
		else{
			dan=randomRange(16, days+1);
		}
		
		//Dobi uro glede na utezi
		int ura=0;
		r=Math.random();
		
		double [] uteziUre=nast.getPropDoubleArr("UTEZI_URE");
		utez=0.0;
		
		for (int i = 0; i < uteziUre.length; i++) {
			
			if(r>=utez && r<utez+uteziUre[i]){
				ura=randomRange(i*6, i*6+6);
				break;
			}
			
			utez+=uteziUre[i];
		}
		
		int minuta=randomRange(0, 60);
		int sekunda=randomRange(0, 60);
		
		d=new GregorianCalendar(leto,mesec,dan,ura,minuta,sekunda);
		
		return d;
	}
	
	private static int nakljucnaStevilka(char c){
		int[] omrezjaMobilna={31,41,51,71,30,40,68,70,64};
		int [] omrezjaStac={1,2,3,4,5,7};
		int [] omrezjaPos={80,89,90};
		int [] omrezjaIP={8200,8201,8202,8205,8209};
		
		if(c=='s'){
			return omrezjaMobilna[randomRange(0, 9)];
		}
		else{
			double r=Math.random();
			double [] uteziOmrezja=nast.getPropDoubleArr("UTEZI_OMREZJA");
			double utez=0.0;
			int ind=0;
			
			for (int i = 0; i < uteziOmrezja.length; i++) {
				
				if(r>=utez && r<utez+uteziOmrezja[i]){
					ind=i;
					break;
				}
				
				utez+=uteziOmrezja[i];
			}
			
			switch (ind) {
			case 0:
				double utezLastnoOmrezje=nast.getPropDouble("UTEZ_LASTNO_OMREZJE");
				r=Math.random();
				
				if(r<utezLastnoOmrezje){
					int[] lastnoOmrezje=Arrays.copyOfRange(omrezjaMobilna, omrezjeUpo[0], omrezjeUpo[1]);
					return lastnoOmrezje[randomRange(0, lastnoOmrezje.length)];
				}
				else{
					int []a=Arrays.copyOfRange(omrezjaMobilna, 0, omrezjeUpo[0]);
					int []b=Arrays.copyOfRange(omrezjaMobilna, omrezjeUpo[1], omrezjaMobilna.length);
					int []combine=new int[a.length+b.length];
					System.arraycopy(a, 0, combine, 0, a.length);
					System.arraycopy(b, 0, combine, a.length, b.length);
					return combine[randomRange(0, combine.length)];
				}
			case 1:
				return omrezjaStac[randomRange(0, 6)];
			case 2:
				return omrezjaPos[randomRange(0, 3)];
			default:
				return omrezjaIP[randomRange(0, 5)];
			}
		}
	}
	
	private static int nakljucnaKolicina(){
		return randomRange(nast.getPropInt("NET_KOLICINA_KB_OBSEG_OD"), nast.getPropInt("NET_KOLICINA_KB_OBSEG_DO")+1);
	}
	
	private static int nakljucnoTrajanje(){
		return randomRange(nast.getPropInt("KLICI_TRAJANJE_OD"), nast.getPropInt("KLICI_TRAJANJE_DO")+1);
	}
	
	private static int randomRange(int min,int max){
		return min + (int)(Math.random() * (max - min));
	}
}
