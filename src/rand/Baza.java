package rand;
import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;


public class Baza {
	
	MongoClient mongoClient;
	DB db;
	DBCollection coll;
	ObjectId idUporabnika;
	String imeOmrezja;
	
	public Baza(String emso) {
		try{
			String host="sandbox.lavbic.net";
			//String host="localhost";
			
			mongoClient = new MongoClient( host , 27017 );
			
			db=mongoClient.getDB("mag-oberstar");
			
			if(!host.equals("localhost")){
				db.authenticate("oberstar", "oberstar".toCharArray());
			}
			
			coll=db.getCollection("podatki");
			
			DBCollection uporabniki=db.getCollection("uporabnik");
			
			idUporabnika=((BasicDBObject)uporabniki.find(new BasicDBObject("emso", emso)).next()).getObjectId("_id");
			
			BasicDBList storitve=(BasicDBList)((BasicDBObject)uporabniki.find(new BasicDBObject("emso", emso)).next()).get("storitev");
			BasicDBObject idStoritveObj=(BasicDBObject)storitve.get(0);
			
			ObjectId idStoritve=idStoritveObj.getObjectId("id_storitve");
			
			DBCollection storitev=db.getCollection("storitev");
			
			imeOmrezja=((BasicDBObject)storitev.find(new BasicDBObject("_id", idStoritve)).next()).getString("operater");
			
			
		}catch(UnknownHostException e){
			e.printStackTrace();
		}catch(MongoException e){
			e.printStackTrace();
		}
		
	}
	
	public int [] omrezjeUporabnika(){
		
		if(imeOmrezja.equals("Mobitel")){
			return new int[]{0,2};
		}
		else if(imeOmrezja.equals("Izimobil")){
			return new int[]{2,3};
		}
		else if(imeOmrezja.equals("Debitel")){
			return new int[]{3,4};
		}
		else if(imeOmrezja.equals("Simobil")){
			return new int[]{4,6};
		}
		else if(imeOmrezja.equals("Bob")){
			return new int[]{6,7};
		}
		else if(imeOmrezja.equals("Tusmobil")){
			return new int[]{7,8};
		}
		else{
			return new int[]{8,9};
		}
	}

	public void vnesiPodatke(Podatki[] p){
		BasicDBObject doc;
		
		for (int i = 0; i < p.length; i++) {
			
			doc = new BasicDBObject();
			
			doc.put("id_uporabnika",idUporabnika);
			doc.put("datum", p[i].getDatum());
			doc.put("lokacija_x", p[i].getLokacija_x());
			doc.put("lokacija_y", p[i].getLokacija_y());
			doc.put("tip", p[i].getTip());
			doc.put("odhodni_dohodni", p[i].getOdhDoh());
			doc.put("stevilka", p[i].getStevilka());
			doc.put("trajanje", p[i].getTrajanje());
			doc.put("kolicina", p[i].getKolicina());
			
			coll.insert(doc);
			
		}
	}
	
	public void disconnect(){
		mongoClient.close();
	}
}
